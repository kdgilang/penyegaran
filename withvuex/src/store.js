import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)
export const state = {
  sitename: 'Search Product',
  apiUri: 'api/products',
  products: [],
  editproducts: [],
  notFound: false
}
export const actions = {
  setProducts: function ({commit}, val) {
    commit('setProducts', val)
  }
}
export const mutations = {
  setProducts: function (state, val) {
    if (val !== '') {
      state.editproducts = val
    }
  }
}
export const getters = {
  getProducts: state => state.products,
  getApiUri: (state, getters) => (path) => {
    return state.apiUri + path
  }
}
export const store = new Vuex.Store({
  state,
  mutations,
  actions,
  getters
})
